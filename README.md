## Résumé
Mini-application **React** pour comparer les librairies **FingerprintJS** et **ClientJS**

## FingerprintJS
- Code source : [Lien Git](https://github.com/fingerprintjs/fingerprintjs)
- Licence : **MIT**
- Dernière release : **v4.0.0 on Jul 26, 2023**
- Contributeurs : **83**
- Fork : **2200**
- Star : **19400**

## ClientJS
- Code source : [Lien Git](https://github.com/jackspirou/clientjs)
- Licence : **Apache Version 2.0**
- Dernière release : **v0.2.1 on Oct 25, 2021**
- Contributeurs : **11**
- Fork : **322**
- Star : **1800**

## Preview
![Capture d'écran du rendu](./public/screen.png)

## Lancement de l'app
A la racine du projet faire :

```
$ npm install
$ npm start
```

## Infos utiles
- Alternant : **Mathis Coué**
- Entreprise : [**Socleo**](https://www.socleo.fr/)
- Date du README : **09/08/2023**
- Langages : **JavaScript**, **HTML/CSS**

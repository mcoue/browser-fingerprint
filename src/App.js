import LibClientJS from './components/LibClientJS';
import LibFingerprintJS from './components/LibFingerprintJS';

const App = () => (
  <>
    <h1>Fingerprint Demo</h1>
    <div className="flex-box">
      <LibFingerprintJS />
      <LibClientJS />
    </div>
  </>
)
export default App;

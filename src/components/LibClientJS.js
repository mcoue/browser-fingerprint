import { ClientJS } from 'clientjs';

const LibClientJS = () => {
    const client  = new ClientJS();
    const ua = client.getUserAgent();
    const canvasPrint = client.getCanvasPrint();

    return (
        <div>
            <h2>ClientJS</h2>
            <ul className="datas">
                <li>Visiteur ID :
                    <ul>
                        <li>{client.getFingerprint()}</li>
                        <li>Custom (UA + canvas): {client.getCustomFingerprint(ua, canvasPrint)}</li>                        
                    </ul>
                </li>
                <li>Navigateur :
                    <ul><li>{client.getBrowser() + " version " + client.getBrowserVersion()}</li></ul>
                </li>
                <li> User Agent :
                    <ul><li>{client.getUserAgent()}</li></ul>
                </li>
                <li> Autorisations :
                    <ul>
                        <li>Stockage local : {client.isLocalStorage().toString()}</li>
                        <li>Stockage session : {client.isSessionStorage().toString()}</li>
                        <li>Accepte les cookies : {client.isCookie().toString()}</li>
                    </ul>
                </li>                    
                <li>Score de confiance <br/> (Chiffre entre 0 et 1. Plus il est haut plus les données récupérées sont fiables) :
                    <ul>
                        <li>???</li>
                    </ul>
                </li>
                <li>Les langues du navigateur :
                    <ul>
                        <li>La langue utilisée :</li>
                        <ul><li>{client.getLanguage()}</li></ul>
                    </ul>
                    <ul>
                        <li>Les langues installées :</li>
                        <ul><li>???</li></ul> 
                    </ul>
                </li>
                <li>Polices du navigateur :
                    <ul>{client.getFonts().slice(0, -3).split(",", 9).map((font, index) => (
                        <li key={index}>{font}</li>))}
                        <li>Je me suis limité à 9 mais 46 sont récupérés</li>
                    </ul>
                </li>
                <li>OS utilisé :
                    <ul>{client.getOS()}</ul>
                </li>
                <li>CPU utilisé :
                    <ul><li>{client.getCPU()}</li></ul>
                </li>
                <li>GPU utilisé :
                    <ul><li>???</li></ul>
                </li>
                <li>Plugin installées :
                    <ul>{client.getPlugins().split(",").map((plugin, index) => (
                        <li key={index}>{plugin}</li>
                    ))}</ul>
                </li>
                <li>Résolution de l'écran :
                    <ul><li>{client.getCurrentResolution()} (disponible : {client.getAvailableResolution()})</li></ul>
                </li>
                <li>Timezone :
                    <ul><li>{client.getTimeZone()}</li></ul>
                </li>
            </ul>
        </div>        
    )
}

export default LibClientJS;
import {useFingerprint} from "./hooks/useFingerprint";

const LibFingerprintJS = () => {
    let [fingerprint] = useFingerprint();

    if (!fingerprint) {
        return null;
    }

    return ( 
        <div>  
            <h2>FingerPrintJS</h2>   
            <ul className="datas">
                <li>Visiteur ID :
                    <ul>
                        <li>{fingerprint.visitorId}</li>
                        <li>Custom: ???</li>
                    </ul>
                </li>
                <li>Navigateur :
                    <ul><li>???</li></ul>
                </li>
                <li> User Agent :
                    <ul><li>???</li></ul>
                </li>
                <li> Autorisations :
                    <ul>
                        <li>Stockage local : {fingerprint.components.localStorage.value.toString()}</li>
                        <li>Stockage session : {fingerprint.components.sessionStorage.value.toString()}</li>
                        <li>Accepte les cookies : {fingerprint.components.cookiesEnabled.value.toString()}</li>
                    </ul>
                </li> 
                <li>Score de confiance <br/> (Chiffre entre 0 et 1. Plus il est haut plus les données récupérées sont fiables) :
                    <ul>
                        <li>{fingerprint.confidence.score}</li>
                        <li>{fingerprint.confidence.comment}</li>
                    </ul>
                </li>
                <li>Les langues du navigateur :
                    <ul>
                        <li>La langue utilisée :</li>
                        <ul><li>{fingerprint.components.languages.value[0]}</li></ul>
                    </ul>
                    <ul>
                        <li>Les langues installées :</li>
                            <ul>
                            {fingerprint.components.languages.value[1].map((langue, index) => (
                                <li key={index}>{langue}</li>
                            ))}
                        </ul>
                    </ul>
                </li>
                <li>Polices du navigateur :
                    <ul>{fingerprint.components.fonts.value.map((font, index) => (
                        <li key={index}>{font}</li>
                    ))}</ul>
                </li>
                <li>OS utilisé :
                    <ul>{fingerprint.components.osCpu.value}</ul>
                </li>
                <li>CPU utilisé :
                    <ul><li>???</li></ul>
                </li>
                <li>GPU utilisé :
                    <ul><li>{fingerprint.components.videoCard.value.renderer}</li></ul>
                </li>
                <li>Plugin installées :
                    <ul>{fingerprint.components.plugins.value.map((plugin, index) => (
                        <li key={index}>{plugin.name} ({plugin.description})</li>
                    ))}</ul>
                </li>
                <li>Résolution de l'écran :
                    <ul><li>{fingerprint.components.screenResolution.value[0]} x {fingerprint.components.screenResolution.value[1]}</li></ul>
                </li>
                <li>Timezone :
                    <ul><li>{fingerprint.components.timezone.value}</li></ul>
                </li>
            </ul>
        </div>
    )
}

export default LibFingerprintJS;
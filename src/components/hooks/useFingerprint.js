import {useEffect, useState} from "react";
import FingerPrintJS from "@fingerprintjs/fingerprintjs";

const fpPromise = FingerPrintJS.load({
    monitoring: false,
});

export const useFingerprint = () => {
    let [fingerprint, setFingerprint] = useState(null);

    useEffect(() => {
        const setFp = async () => {
            const fp = await fpPromise;
            const fingerprint = await fp.get();

            setFingerprint(fingerprint);
        };

        setFp();
    }, []);

    return [fingerprint];
}